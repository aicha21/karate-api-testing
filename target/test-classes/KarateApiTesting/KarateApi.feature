Feature: Test User API

  Scenario Outline: api content
    Given url "http://debug-api.demo.ecom.gl.rocks/content/v1/pages/home/<page>"
    And header X-GGL-CONTEXT-TENANT = 'gl'
    When method GET
    Then status 200
    And match $ contains {id:<page>}

    Examples:
    | page   |
    | femme |
    | homme  |

  Scenario: BFF
    * text query =
    """
    query( $preview: Boolean, $datetime: String) {
          page(id: "femme", locale: "fr", preview: $preview, datetime: $datetime, type: "home", tenant: "gl") {
            title
            id
            tagCategories
            components {
              name
              ... on Header {
                searchBar {
                  suggestions {
                    title
                    links {
                      label
                      url
                    }
                  }
                  noResult {
                    title
                    message
                    thumbnails {
                      label
                      image {
                        url
                        alt
                      }
                      targetUrl
                    }
                  }
                }
              }
              ... on ZoneHeroImage {
                title
                subtitle
                image {
                  alt
                  url
                }
                color
                background
                variant
              }
              ... on Corps {
                text {
                  contents {
                    text
                    marks
                  }
                }
                chapo {
                  contents {
                    text
                    marks
                  }
                }
                textPosition
              }
              ... on Stacker {
                universeTag
                stackerLines {
                  title
                  subtitle
                  legalNotices
                  ctas {
                    link
                    text
                    color
                  }
                  image {
                    alt
                    url
                  }
                }
              }
              ... on SinglePushEdito {
                title
                subtitle
                color
                background
                banner
                cta {
                  link
                  text
                  color
                }
                images {
                  type
                  alt
                  url
                }
              }
              ... on PushCategories {
                bannerText
                categories {
                  label
                  description
                  url
                  image {
                    url
                  }
                }
                newsZone {
                  title
                  cta {
                    link
                    text
                  }
                }
              }
              ... on Reinsurance {
                reinsuranceMessages {
                  image {
                    alt
                    url
                  }
                  title
                  link {
                    label
                    url
                    scramble
                  }
                }
              }
              ... on UniverseNavigation {
                tagline
                universes {
                  link {
                    label
                    url
                    scramble
                  }
                  images {
                    alt
                    url
                  }
                }
              }
              ... on Menu {
                universes {
                  title {
                    label
                    url
                    scramble
                  }
                  color
                  subUniverses {
                    title {
                      label
                      url
                      scramble
                    }
                    categories {
                      title {
                        label
                        url
                        scramble
                      }
                      new
                    }
                  }
                  sidebar {
                    title {
                      label
                      url
                      scramble
                    }
                    categories {
                      title {
                        label
                        url
                        scramble
                      }
                    }
                    image {
                      alt
                      url
                    }
                  }
                }
              }
            }
            seo {
              title
              metaDescription
            }
          }
        }
    """
    Given url 'https://ggl-bff.demo.ecom.gl.rocks/graphql'
    When request {query: '#(query)'}
    When method post
    Then status 200

    #* def myJson = response
    #* print 'the value of myJson is:', myJson
   * match $.data.page.title == 'Femme'
