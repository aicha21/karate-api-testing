$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/KarateApiTesting/KarateApi.feature");
formatter.feature({
  "line": 1,
  "name": "Test User API",
  "description": "",
  "id": "test-user-api",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "api content",
  "description": "",
  "id": "test-user-api;api-content",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "url \"http://debug-api.demo.ecom.gl.rocks/content/v1/pages/home/\u003cpage\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "header X-GGL-CONTEXT-TENANT \u003d \u0027gl\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "method GET",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "status 200",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "match $ contains {id:\u003cpage\u003e}",
  "keyword": "And "
});
formatter.examples({
  "line": 10,
  "name": "",
  "description": "",
  "id": "test-user-api;api-content;",
  "rows": [
    {
      "cells": [
        "page"
      ],
      "line": 11,
      "id": "test-user-api;api-content;;1"
    },
    {
      "cells": [
        "femme"
      ],
      "line": 12,
      "id": "test-user-api;api-content;;2"
    },
    {
      "cells": [
        "homme"
      ],
      "line": 13,
      "id": "test-user-api;api-content;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 12,
  "name": "api content",
  "description": "",
  "id": "test-user-api;api-content;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "url \"http://debug-api.demo.ecom.gl.rocks/content/v1/pages/home/femme\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "header X-GGL-CONTEXT-TENANT \u003d \u0027gl\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "method GET",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "status 200",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "match $ contains {id:femme}",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "\"http://debug-api.demo.ecom.gl.rocks/content/v1/pages/home/femme\"",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 801623713,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "X-GGL-CONTEXT-TENANT",
      "offset": 7
    },
    {
      "val": "\u0027gl\u0027",
      "offset": 30
    }
  ],
  "location": "StepDefs.header(String,String\u003e)"
});
formatter.result({
  "duration": 12855438,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 7
    }
  ],
  "location": "StepDefs.method(String)"
});
formatter.result({
  "duration": 509942777,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 518589,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {},
    {
      "val": "$",
      "offset": 6
    },
    {},
    {},
    {},
    {
      "val": " {id:femme}",
      "offset": 16
    }
  ],
  "location": "StepDefs.matchContains(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 8075464,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "api content",
  "description": "",
  "id": "test-user-api;api-content;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "url \"http://debug-api.demo.ecom.gl.rocks/content/v1/pages/home/homme\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "header X-GGL-CONTEXT-TENANT \u003d \u0027gl\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "method GET",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "status 200",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "match $ contains {id:homme}",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "\"http://debug-api.demo.ecom.gl.rocks/content/v1/pages/home/homme\"",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 85998399,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "X-GGL-CONTEXT-TENANT",
      "offset": 7
    },
    {
      "val": "\u0027gl\u0027",
      "offset": 30
    }
  ],
  "location": "StepDefs.header(String,String\u003e)"
});
formatter.result({
  "duration": 7843909,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 7
    }
  ],
  "location": "StepDefs.method(String)"
});
formatter.result({
  "duration": 754812199,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 85813,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {},
    {
      "val": "$",
      "offset": 6
    },
    {},
    {},
    {},
    {
      "val": " {id:homme}",
      "offset": 16
    }
  ],
  "location": "StepDefs.matchContains(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 526137,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "BFF",
  "description": "",
  "id": "test-user-api;bff",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 16,
  "name": "text query \u003d",
  "keyword": "* ",
  "doc_string": {
    "content_type": "",
    "line": 17,
    "value": "query( $preview: Boolean, $datetime: String) {\n      page(id: \"femme\", locale: \"fr\", preview: $preview, datetime: $datetime, type: \"home\", tenant: \"gl\") {\n        title\n        id\n        tagCategories\n        components {\n          name\n          ... on Header {\n            searchBar {\n              suggestions {\n                title\n                links {\n                  label\n                  url\n                }\n              }\n              noResult {\n                title\n                message\n                thumbnails {\n                  label\n                  image {\n                    url\n                    alt\n                  }\n                  targetUrl\n                }\n              }\n            }\n          }\n          ... on ZoneHeroImage {\n            title\n            subtitle\n            image {\n              alt\n              url\n            }\n            color\n            background\n            variant\n          }\n          ... on Corps {\n            text {\n              contents {\n                text\n                marks\n              }\n            }\n            chapo {\n              contents {\n                text\n                marks\n              }\n            }\n            textPosition\n          }\n          ... on Stacker {\n            universeTag\n            stackerLines {\n              title\n              subtitle\n              legalNotices\n              ctas {\n                link\n                text\n                color\n              }\n              image {\n                alt\n                url\n              }\n            }\n          }\n          ... on SinglePushEdito {\n            title\n            subtitle\n            color\n            background\n            banner\n            cta {\n              link\n              text\n              color\n            }\n            images {\n              type\n              alt\n              url\n            }\n          }\n          ... on PushCategories {\n            bannerText\n            categories {\n              label\n              description\n              url\n              image {\n                url\n              }\n            }\n            newsZone {\n              title\n              cta {\n                link\n                text\n              }\n            }\n          }\n          ... on Reinsurance {\n            reinsuranceMessages {\n              image {\n                alt\n                url\n              }\n              title\n              link {\n                label\n                url\n                scramble\n              }\n            }\n          }\n          ... on UniverseNavigation {\n            tagline\n            universes {\n              link {\n                label\n                url\n                scramble\n              }\n              images {\n                alt\n                url\n              }\n            }\n          }\n          ... on Menu {\n            universes {\n              title {\n                label\n                url\n                scramble\n              }\n              color\n              subUniverses {\n                title {\n                  label\n                  url\n                  scramble\n                }\n                categories {\n                  title {\n                    label\n                    url\n                    scramble\n                  }\n                  new\n                }\n              }\n              sidebar {\n                title {\n                  label\n                  url\n                  scramble\n                }\n                categories {\n                  title {\n                    label\n                    url\n                    scramble\n                  }\n                }\n                image {\n                  alt\n                  url\n                }\n              }\n            }\n          }\n        }\n        seo {\n          title\n          metaDescription\n        }\n      }\n    }"
  }
});
formatter.step({
  "line": 205,
  "name": "url \u0027https://ggl-bff.demo.ecom.gl.rocks/graphql\u0027",
  "keyword": "Given "
});
formatter.step({
  "line": 206,
  "name": "request {query: \u0027#(query)\u0027}",
  "keyword": "When "
});
formatter.step({
  "line": 207,
  "name": "method post",
  "keyword": "When "
});
formatter.step({
  "line": 208,
  "name": "status 200",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 210,
      "value": "#* def myJson \u003d response"
    },
    {
      "line": 211,
      "value": "#* print \u0027the value of myJson is:\u0027, myJson"
    }
  ],
  "line": 212,
  "name": "match $.data.page.title \u003d\u003d \u0027Femme\u0027",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "query",
      "offset": 5
    }
  ],
  "location": "StepDefs.textDocString(String,String)"
});
formatter.result({
  "duration": 49653397,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027https://ggl-bff.demo.ecom.gl.rocks/graphql\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 7528161,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{query: \u0027#(query)\u0027}",
      "offset": 8
    }
  ],
  "location": "StepDefs.request(String)"
});
formatter.result({
  "duration": 8996831,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "post",
      "offset": 7
    }
  ],
  "location": "StepDefs.method(String)"
});
formatter.result({
  "duration": 394504174,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 71359,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {},
    {
      "val": "$.data.page.title",
      "offset": 6
    },
    {},
    {
      "val": "\u003d\u003d",
      "offset": 24
    },
    {
      "val": "\u0027Femme\u0027",
      "offset": 27
    }
  ],
  "location": "StepDefs.matchEquals(String,String,String,String,String)"
});
formatter.result({
  "duration": 12488702,
  "status": "passed"
});
});